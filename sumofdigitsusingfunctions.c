#include<stdio.h>
int input()
{
    int a;
    printf("Enter a number\n");
    scanf("%d",&a);
    return a;
}
int compute(int a)
{
    int i,sum=0;
    while(a!=0)
    {
        i=a%10;
        sum=sum+i;
        a=a/10;
    }
    return sum;
}

void output(int sum)
{
    printf("Sum of digits=%d\n",sum);
}

int main()
{
    int a,sum;
    a=input();
    sum=compute(a);
    output(sum);
    return 0;
}
