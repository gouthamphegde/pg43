#include<stdio.h>
int main()
{
    int n,rev=0,rem;
    printf("Enter a number\n");
    scanf("%d",&n);
    for(;n!=0;n/=10)
    {
        rem=n%10;
        rev=rev*10+rem;
    }
    printf("Reverse=%d\n",rev);
    if(rev==n)
    {
        printf("The number is a palindrome\n");
    }
    else printf("The number is not a palindrome\n");
    return 0;
}