#include<stdio.h>
#include<math.h>
int input()
{
    int a;
    printf("Enter a number\n");
    scanf("%d",&a);
    return a;
}
int compute(int b)
{
    int i,sum=0;
    for(i=1;i<=b;i++)
    {
        int a;
        a=pow(i,2);
        sum=sum+a;
    }
    return sum;
}
void output(int sum)
{
    printf("Sum of squares of numbers=%d\n",sum);
}
int main()
{
    int a,sum;
    a=input();
    sum=compute(a);
    output(sum);
    return 0; 
}
    