#include <stdio.h>

int main ()
{
   int c = 0;
   char cha,s[500];

   printf("Input a string\n");
   gets(s);

   while (s[c] != '\0') {
      cha=s[c];
      if (cha >= 'A' && cha <= 'Z')
         s[c] = s[c] + 32;
      else if (cha >= 'a' && cha <= 'z')
         s[c] = s[c] - 32;
      c++;
   }

   printf("%s\n", s);

   return 0;
}
