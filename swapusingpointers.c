#include<stdio.h>
void swap(int*x,int*y)
{
    int temp;
    temp=*x;
    *x=*y;
    *y=temp;
}
int main()
{
    int a,b;
    printf("Enter first number a=");
    scanf("%d",&a);
    printf("Enter second number b=");
    scanf("%d",&b);
    swap(&a,&b);
    printf("After swap\n");
    printf("a=%d b=%d\n",a,b);
    return 0;
}
